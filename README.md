# Bent Normals Baker for Unity

## How to use
  + Export an object in OBJ format.
  + Run this software on the OBJ file.
  + Import Bent Normals and AO in Unity for use.

It is possible to use a high resolution mesh to build the AO and BN maps. You only have to ensure that the UV space matches between the high and low poly models.

## Frequently encountered issues
  + **Do not** import texture as compressed in Unity. Unity's BCH6 compression is very bad for normal maps.
  + You might have to tweak the export space if your modeler switches 'x' sign (Blender does by default).