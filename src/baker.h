#pragma once

// STL includes
#define _USE_MATH_DEFINES
#include <cmath>
#include <random>
#include <chrono>
#include <omp.h>

// TinyOBJ includes
#include <tinyobj/tiny_obj_loader.h>

// Embree includes
#include <embree2/rtcore.h>
#include <embree2/rtcore_ray.h>

// GLM includes
#include <glm/glm.hpp>

// Local includes
#include "image.h"

//#define USE_RAY4_PACKET
//#define USE_RAY16_PACKET

#if defined(USE_RAY16_PACKET)
    const int RAYS_PER_PACKET = 16;
#elif defined(USE_RAY4_PACKET)
    const int RAYS_PER_PACKET = 4;
#else
    const int RAYS_PER_PACKET = 1;
#endif

namespace baker {

    /* Permits to specify a configuration for the Baker
     */
    struct baker_config {
        int _spp;
        int _passes;
    };

    /* Baker: compute data in the UV domain
     */
    struct baker_bentcone {

        baker_bentcone(const baker_config& config,
                       const std::vector<tinyobj::shape_t>& shapes,
                       const tinyobj::attrib_t& attrib) {

            // Update configuration
            _config = config;

            // Create EMBREE scene
            device = rtcNewDevice(NULL);
            if(rtcDeviceGetError(device) != RTC_NO_ERROR) {
                std::cerr << "ERROR: could not create EMBREE device object" << std::endl;
                return;
            }

#if defined(USE_RAY4_PACKET)
            scene = rtcDeviceNewScene(device, RTC_SCENE_STATIC, RTC_INTERSECT4);
#else
            scene = rtcDeviceNewScene(device, RTC_SCENE_STATIC, RTC_INTERSECT1);
#endif
            if(rtcDeviceGetError(device) != RTC_NO_ERROR) {
                std::cerr << "ERROR: could not create EMBREE scene object" << std::endl;
                return;
            }

            // Fill the EMBREE scene with TinyOBJ
            for(auto shape : shapes) {

                // Create triangle mesh in EMBREE
                unsigned int nf = shape.mesh.num_face_vertices.size();
                unsigned int nv = attrib.vertices.size() / 3;
                unsigned int id = rtcNewTriangleMesh(scene, RTC_GEOMETRY_STATIC, nf, nv);

                // Add the vertices to EMBREE
                glm::vec4* vertices = (glm::vec4*) rtcMapBuffer(scene, id, RTC_VERTEX_BUFFER);
                for (unsigned int f = 0; f<nv; f++) {
                    vertices[f].x = attrib.vertices[3*f+0];
                    vertices[f].y = attrib.vertices[3*f+1];
                    vertices[f].z = attrib.vertices[3*f+2];
                }
                rtcUnmapBuffer(scene, id, RTC_VERTEX_BUFFER);

                // Add triangle indices
                glm::ivec3* triangles = (glm::ivec3*) rtcMapBuffer(scene, id, RTC_INDEX_BUFFER);
                size_t index_offset = 0;
                for (unsigned int f = 0; f<nf; f++) {
                    int fv = shape.mesh.num_face_vertices[f];

                    // Raster the triangle in face
                    const tinyobj::index_t& p1 = shape.mesh.indices[index_offset + 0];
                    const tinyobj::index_t& p2 = shape.mesh.indices[index_offset + 1];
                    const tinyobj::index_t& p3 = shape.mesh.indices[index_offset + 2];
                    triangles[f].x = p1.vertex_index;
                    triangles[f].y = p2.vertex_index;
                    triangles[f].z = p3.vertex_index;

                    index_offset += fv;
                }
                rtcUnmapBuffer(scene, id, RTC_INDEX_BUFFER);
            }

            rtcCommit(scene);

        }

        ~baker_bentcone() {
            rtcDeleteDevice(device);
            if(_dither != nullptr) {
                delete _dither;
            }
        }

        /* Load a dither image to shift the samples
         */
        void load_dither(std::string& filename) {
            _dither = new baker::image(filename);
        }

//#define FIBONACCI_SEQUENCE

        /* Generate a sequence of `num_samples` pseudo-random directions on the
         * hemisphere. These directions will later be used by the integrator.
         */
        std::vector<glm::vec2> generate_samples(unsigned int num_samples,
                                                unsigned int offset) const {
            std::vector<glm::vec2> wls(num_samples);
#ifdef FIBONACCI_SEQUENCE
            static const float Phi = sqrt(0.5)*0.5 + 0.5;
            const float inv_n = 1.0 / float(num_samples);
            for(unsigned int k=0; k<num_samples; ++k) {
               float y = k*Phi;
               y = y - floor(y);
               float x = 1.0 - (2.0*k+1.0) * inv_n;
               wls[k] = glm::vec2(x, y);
            }
#else
            std::default_random_engine rand_gen(offset);
            std::uniform_real_distribution<float> rand_dist(0.f, 1.f);

            for(unsigned int k=0; k<num_samples; ++k) {
               wls[k] = glm::vec2(rand_dist(rand_gen), rand_dist(rand_gen));
            }
#endif
            return wls;
        }

        glm::vec3 generateDirectionCosine(glm::vec2 sample) const {
            float r1 = 2.0f*sample.x - 1.0f;
            float r2 = 2.0f*sample.y - 1.0f;

            /* Modified concencric map code with less branching (by Dave Cline), see
            http://psgraphics.blogspot.ch/2011/01/improved-code-for-concentric-map.html */
            float phi, r;
            if (r1 == 0 && r2 == 0) {
                r = phi = 0;
            } else if (r1*r1 > r2*r2) {
                r = r1;
                phi = (M_PI/4.0f) * (r2/r1);
            } else {
                r = r2;
                phi = (M_PI/2.0f) - (r1/r2) * (M_PI/4.0f);
            }

            float sinPhi = sin(phi);
            float cosPhi = cos(phi);

            return glm::vec3(r * cosPhi, r * sinPhi, sqrtf(1.0f - r*r));
        }

        glm::vec3 generateDirectionUniform(glm::vec2 sample) const {
            float r1 = 2.0f*sample.x - 1.0f;
            float r2 = 2.0f*sample.y - 1.0f;

            /* Modified concencric map code with less branching (by Dave Cline), see
            http://psgraphics.blogspot.ch/2011/01/improved-code-for-concentric-map.html */
            float phi, r, sr;
            if (r1 == 0 && r2 == 0) {
                r = phi = 0;
            } else if (r1*r1 > r2*r2) {
                r = r1;
                phi = (M_PI/4.0f) * (r2/r1);
            } else {
                r = r2;
                phi = (M_PI/2.0f) - (r1/r2) * (M_PI/4.0f);
            }
            r2 = r*r;
            sr = sqrtf(2.0f-r2);
            float sinPhi = sin(phi);
            float cosPhi = cos(phi);

            return glm::vec3(r*sr * cosPhi, r*sr * sinPhi, 1.0f - r2);
        }

        /* Bake an image
         */
        void bake(const baker::image& img_p, const baker::image& img_n,
                  baker::image* img_ao, baker::image* img_bn) const {

            // Constants
            const int W = img_n.width();
            const int H = img_n.height();

            std::chrono::high_resolution_clock::time_point t_start, t_end;
            t_start = std::chrono::high_resolution_clock::now();

            // Generate a dither mask for the all image if no one has been provided
            std::default_random_engine rand_gen(1295721945);
            std::uniform_real_distribution<float> rand_dist(0.f, 1.f);
            if(_dither == nullptr) {
                _dither = new baker::image(W, H, baker::image::RGB);
                for(int i=0; i<W*H; ++i) {
                    _dither->_pix[i*3 + 0]= rand_dist(rand_gen);
                    _dither->_pix[i*3 + 1]= rand_dist(rand_gen);
                    _dither->_pix[i*3 + 2]= rand_dist(rand_gen);
                }
            }

            // Recalculate the number of required SPP with respect to vectorization
            const int num_spp_packet = (_config._spp/RAYS_PER_PACKET);
            const int num_samples_per_pass = RAYS_PER_PACKET * num_spp_packet;
            std::cout << "[DEBUG] Baking AO and BN using " << num_samples_per_pass << " spp" << std::endl;
            std::cout << "[DEBUG] Vectorization will use " << num_spp_packet << " calls to rtOcclude" << std::endl;

            // Create a tmp image to store the number of samples used per
            // pass for adaptive sampling.
            baker::image spp_img(img_n.width(), img_n.height(), baker::image::RGB);
            for(int j=0; j<img_n.height(); ++j) {
                for(int i=0; i<img_n.width(); ++i) {
                    spp_img(i, j, 0) = (float)num_samples_per_pass; // number sample for pass for pixel i,j
                    spp_img(i, j, 1) = (float)0.f;                  // total number of samples
                    spp_img(i, j, 2) = (float)0.f;                  // cumulant k_2
                }
            }

            const int width  = img_n.width();
            const int height = img_n.height();


#define COMPUTE_PASS_VARIANCE
// #define COMPUTE_SAMPLE_VARIANCE

            int nb_passes = _config._passes;
            for(unsigned int pass=0; pass<nb_passes; ++pass) {

                std::cout << "Pass number " << pass+1 << std::endl;

                // Compute the max of the variance
                float max_var = 0.0f;
                for(int j=0; j<height; ++j) {
                    for(int i=0; i<width; ++i) {
#if defined(COMPUTE_SAMPLE_VARIANCE)
                        max_var = std::max<float>(max_var, spp_img(i, j, 2));
#elif defined(COMPUTE_PASS_VARIANCE)
                        const float mean = (*img_ao)(i, j, 0) / spp_img(i, j, 1);
                        const float k_2  = spp_img(i, j, 2);
                        const float s_2  = k_2;// - mean*mean;
                        max_var = std::max<float>(max_var, s_2);
#endif
                    }
                }

                assert(max_var >= 0.f);

                // Loop over all the pixels and perform baking there
                #pragma omp parallel for
                for(int j=0; j<height; ++j) {
                    for(int i=0; i<width; ++i) {

                        // Compute the current number of samples in the thread
                        const float mean      = (*img_ao)(i, j, 0) / spp_img(i, j, 1);
#if defined(COMPUTE_SAMPLE_VARIANCE)
                        const float sigma     = sqrtf(spp_img(i, j, 2) / spp_img(i, j, 1));
                        const float var_scale = std::min(1.0f, spp_img(i, j, 2) / max_var);
                        const int num_samples = (int) floor( num_samples_per_pass * var_scale);
                        if(num_samples == 0) { continue; }

                        const float threshold = 0.05 / 1.96;
                        if(sigma < mean*threshold) { continue; }
#elif defined(COMPUTE_PASS_VARIANCE)
                        float k_2   = spp_img(i, j, 2);
                        const float s_2   = k_2;// - mean*mean;
                        const float sigma = sqrtf(s_2);
                        const float var_scale = (max_var <= 0.0f) ? 1.0f : std::min(1.0f, s_2 / max_var);

                        const int num_samples = (int) floor( num_samples_per_pass * var_scale);
                        if(num_samples == 0) { continue; }
#endif


                        // Pre-compute a distribution of point on the hemisphere
                        std::vector<glm::vec2> ruv_2d = generate_samples(num_samples, 49321*(pass+1));

                        // Random directions generator
                        glm::vec2 shift;
                        int ii = i % _dither->width();
                        int jj = j % _dither->height();
                        float* dither = (*_dither)(ii, jj);
                        shift = glm::vec2(dither[0], dither[1]);

                        // Get position and normal
                        glm::vec3 p = img_p.vector(i, j);
                        glm::vec3 n = img_n.normal(i, j);
                        float length_n = glm::length(n);
                        if(std::abs(length_n - 1.f) >= 0.01f) {
                            img_ao->writeVector(i, j, glm::vec3(0.0f, 0.0f, 0.0f));
                            img_bn->writeNormal(i, j, glm::vec3(0.0f, 0.0f, 0.0f));
                            continue;
                        }
                        n = glm::normalize(n);

                        // Generate local frame
                        glm::vec3 t, b;
                        get_local_frame(n, &t, &b);

                        // Move ray away from surface
                        p = p + 1.0E-6f * n;

                        // Sample ambient occlusion rays using the t,b,n frame.
                        float ao  = 0.f;
                        glm::vec3 bn(0.f, 0.f, 0.f);

                        for(unsigned int k=0; k<num_samples; ++k) {

                            glm::vec3 wl = generateDirectionUniform(glm::mod(ruv_2d[k] + shift, 1.0f));
                            glm::vec3 wo = wl.x*t + wl.y*b + wl.z*n;
                            if(std::abs(glm::length(wo)-1.0f) >= 0.01f) {
                                continue;
                            }
                            wo = glm::normalize(wo);

                            // Create the ray
                            RTCRay ray;
                            ray.org[0] =  p.x;
                            ray.org[1] =  p.y;
                            ray.org[2] =  p.z;
                            ray.dir[0] = wo.x;
                            ray.dir[1] = wo.y;
                            ray.dir[2] = wo.z;
                            ray.geomID = RTC_INVALID_GEOMETRY_ID;
                            ray.tnear = 0.f;
                            ray.tfar  = std::numeric_limits<float>::max();

                            // Shoot ray and give hit point
                            rtcOccluded(scene, ray);
                            if(ray.geomID == RTC_INVALID_GEOMETRY_ID) {
                                ao += 1.f;
                            }
                        }

                        // Update the number of samples
                        const float old_samples = spp_img(i, j, 1);
                        const float tot_samples = spp_img(i, j, 1) + (float)num_samples;
                        spp_img(i, j, 1) = tot_samples;


                        const float old_ao = (old_samples <= 0) ? 0.0f : (*img_ao)(i, j, 0) / old_samples;
                        const float new_ao = ((*img_ao)(i, j, 0) + ao) / tot_samples;

                        const float old_cum_ao = (*img_ao)(i, j, 0);
                        (*img_ao)(i, j, 0) = old_cum_ao + ao;
                        (*img_ao)(i, j, 1) = old_cum_ao + ao;
                        (*img_ao)(i, j, 2) = old_cum_ao + ao;

#if defined(COMPUTE_SAMPLE_VARIANCE)
                        ao = (*img_ao)(i, j, 0) / spp_img(i, j, 1);
                        spp_img(i, j, 2) = ao - ao*ao;
#elif defined(COMPUTE_PASS_VARIANCE)
                        // k_2 = (old_samples*spp_img(i, j, 2) + ao*ao) / spp_img(i, j, 1);
                        const float diff_ao = (old_ao - new_ao);
                        k_2 = diff_ao*diff_ao;

                        if(pass > 0) {
                            spp_img(i, j, 2) = (old_samples*spp_img(i, j, 2) + k_2) / spp_img(i, j, 1);
                        }
#endif
                    }
                }

#if defined(COMPUTE_PASS_VARIANCE)
                // Export variance
                baker::image tmp_img(width, height, baker::image::RGB);
                for(int j=0; j<height; ++j) {
                    for(int i=0; i<width; ++i) {
                        const float mean = (*img_ao)(i, j, 0) / spp_img(i, j, 1);

                        float temp = 0.0f;
                        const int W = 2;
                        for(int u=-W; u<=W; ++u) {
                            for(int v=-W; v<=W; ++v) {
                                const int ii = std::min<int>(width-1,  std::max<int>(0, i+u));
                                const int jj = std::min<int>(height-1, std::max<int>(0, j+v));
                                temp += spp_img(ii, jj, 2);
                            }
                        }
                        temp /= 5*5;
                        spp_img(i, j, 2) = temp;

                        const float k_2  = spp_img(i, j, 2);
                        const float s_2  = k_2;// - mean*mean;
                        tmp_img(i, j, 0) = k_2;
                        tmp_img(i, j, 1) = k_2;
                        tmp_img(i, j, 2) = k_2;
                    }
                }
                tmp_img.save("variance_" + std::to_string(pass+1) + ".exr");
                // exit(0);

                for(int j=0; j<height; ++j) {
                    for(int i=0; i<width; ++i) {
                        tmp_img(i, j, 0) = (*img_ao)(i, j, 0) / spp_img(i, j, 1);
                        tmp_img(i, j, 1) = (*img_ao)(i, j, 0) / spp_img(i, j, 1);
                        tmp_img(i, j, 2) = (*img_ao)(i, j, 0) / spp_img(i, j, 1);
                    }
                }
                tmp_img.save("temp_" + std::to_string(pass+1) + ".exr");
#endif
            }

            // Normalize the image by the number of samples and save the number
            // of samples to a grayscale image.
            for(int j=0; j<height; ++j) {
                for(int i=0; i<width; ++i) {
                    (*img_ao)(i, j, 0) /= spp_img(i, j, 1);
                    (*img_ao)(i, j, 1) /= spp_img(i, j, 1);
                    (*img_ao)(i, j, 2) /= spp_img(i, j, 1);
                    spp_img(i, j, 0) = spp_img(i, j, 1) / (float)(num_samples_per_pass*nb_passes);
                    spp_img(i, j, 2) = spp_img(i, j, 1) / (float)(num_samples_per_pass*nb_passes);
                    spp_img(i, j, 1) = spp_img(i, j, 1) / (float)(num_samples_per_pass*nb_passes);
                }
            }
            spp_img.save("samples.exr");


            t_end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> t_delta = t_end - t_start;
            std::cout << "[TIME] Baking took: " << t_delta.count() << "s" << std::endl;

        }

        /* Generate the local coordinate frame at the point of interest
         * Create as well the transformation matrix.
         * {TODO: the fetched t,b can be ill defined if pixel maps have zeros}
         */
        void generate_frame(const glm::vec3& p, const glm::vec3& n,
                            const baker::image& img_p, int x, int y,
                            glm::vec3* t_out, glm::vec3* b_out) const {

            const int x1 = std::min(x+1, img_p.width()-1);
            const int y2 = std::min(y+1, img_p.height()-1);
            const glm::vec3 p1 = img_p.vector(x1, y);
            const glm::vec3 p2 = img_p.vector(x, y2);
            glm::vec3 t = p1 - p;
            glm::vec3 b = p2 - p;

            /* Orthonormalize frame */
            if(glm::length(t) > 1.0e-9) {
                t = glm::normalize(t - glm::dot(t, n) * n);
                b = glm::cross(n, t);
            } else {
                if(n.y > 0.1) {
                    t = glm::vec3(0.f, -n.z, n.y);
                } else {
                    t = glm::vec3(n.z, 0.f, -n.x);
                }
                b = glm::cross(n, t);
            }

            /* Check frame consistency */
            if (glm::dot(glm::cross(t, b), n) < 0.0) {
                b = -b;
            }

            *t_out = t;
            *b_out = b;
        }

      /* Generate a coordinate frame for a given `localZ` matrix.
      */
      void get_local_frame(const glm::vec3& localZ,
                      glm::vec3* localX, glm::vec3* localY) const {
         glm::vec3 upVector = abs(localZ.z) < 0.999 ? glm::vec3(0.0, 0.0, 1.0) : glm::vec3(1.0, 0.0, 0.0);
         *localX = glm::normalize(glm::cross(upVector, localZ));
         *localY = glm::cross(localZ, *localX);
      }


      /* Dither image for the blue noise dithering method
      */
      mutable baker::image* _dither;

        /* Ray tracing data using EMBREE
         */
        RTCDevice device;
        RTCScene  scene;

        /* Configuration object
         */
        baker_config _config;
    };
}
