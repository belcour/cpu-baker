#pragma once

// GLM includes
#include <glm/glm.hpp>

// TinyEXR implementation
#define TINYEXR_IMPLEMENTATION
#include <tinyexr/tinyexr.h>

namespace baker {
struct image {

    enum type {
        FLOAT,
        RGB,
		RGBA
    };

    /* Constructor
     */
    image(int width, int height, image::type t = image::RGB) : _width(width), _height(height) {
        // Set the number of channels (floats per pixel)
        _fpp = 3;
        if(t == image::FLOAT) {
            _fpp = 1;
		} else if (t == image::RGBA) {
			_fpp = 4;
		}

        // Create buffer
        _pix = new float[_fpp * _width * _height];
        memset(_pix, _fpp*_width*_height*sizeof(float), 0);
    }
    image(const image& img) : _width(img._width), _height(img._height), _fpp(img._fpp) {
        _pix = new float[_fpp * _width * _height];
        memcpy(_pix, img._pix, _width*_height*_fpp*sizeof(float));
    }
    image(const std::string& fname) {
         const char* err;
         _fpp = 4;
         int ret = LoadEXR(&_pix, &_width, &_height, fname.c_str(), &err);
         if(ret != 0) {
            std::cerr << "UNABLE TO LOAD IMAGE" << std::endl;
            throw std::exception();
         }
         std::cout << "Loaded an image of size (" << _width << ", " << _height << ")" << std::endl;
      }

    ~image() {
        delete[] _pix;
    }

    /* Access a pixel
     */
    float* operator()(int i, int j) const {
        assert(i >= 0 && i < _width);
        assert(j >= 0 && j < _height);

        const int jr = _height - (j+1); // reverse the vertical coordinates
        const int index = _fpp*(i + jr*_width);
        return _pix + index;
    }
    float& operator()(int i, int j, int k) const {
        assert(k >= 0 && k < _fpp);

        float* ptr = (*this)(i, j);
        return ptr[k];
    }

    /* Access to a vector or normal at a given pixel
     */
    inline glm::vec3 vector(int i, int j) const {
        assert(i >= 0 && i < _width);
        assert(j >= 0 && j < _height);
        const float* ptr = (*this)(i,j);
        return glm::vec3(ptr[0], ptr[1], ptr[2]);
    }
    inline glm::vec3 normal(int i, int j) const {
        assert(i >= 0 && i < _width);
        assert(j >= 0 && j < _height);
        const float* ptr = (*this)(i,j);
        return glm::vec3(2.f*ptr[0]-1.f, 2.f*ptr[1]-1.f, 2.f*ptr[2]-1.f);
    }

    /* Clear the content of an image
     */
     void clear() {
         memset(_pix, _fpp*_width*_height*sizeof(float), 0);
     }

    /* Write a vector or a normal in a given pixel
     */
    inline void writeVector(int i, int j, const glm::vec3& x) const {
        float* ptr = (*this)(i,j);
        ptr[0] = x.x;
        ptr[1] = x.y;
        ptr[2] = x.z;
    }
    inline void writeNormal(int i, int j, const glm::vec3& x) const {
        float* ptr = (*this)(i,j);
        ptr[0] = 0.5f*x.x + 0.5f;
        ptr[1] = 0.5f*x.y + 0.5f;
        ptr[2] = 0.5f*x.z + 0.5f;
    }

    /* Save an image to disk using TinyEXR
     */
    void save(const std::string& filename, bool swapX=false) const {
        float* tmp = new float[_width*_height*_fpp];
        std::memcpy(tmp, _pix, _width*_height*_fpp*sizeof(float));

        // Reverse the coordinate system to match Unity
        if(swapX) {
            for(int i=0; i<_width*_height; ++i) {
                tmp[_fpp*i + 0] = -1.0f*tmp[_fpp*i + 0] + 1.0;
            }
        }

        SaveEXR(tmp,  _width,  _height,  _fpp, filename.c_str());
        delete[] tmp;
    }


    /* Inflat the invalid content of a picture 
     */
    void inflate(int radius) {
        image tmp(*this);
        #pragma omp parallel for
        for(int i=0; i<_width; ++i) {
            for(int j=0; j<_height; ++j) {
                inflate_loop(i, j, tmp, radius);
            }
        }
        memcpy(_pix, tmp._pix, _width*_height*_fpp*sizeof(float));
    }
    
    void inflate_loop(int i0, int j0, const image& tmp, int radius) {
        // If the central pixel is not null, do not follow this loop
        float* dest = tmp(i0, j0);
        float* src0 = (*this)(i0, j0);
        if(src0[0] != 0.0f && src0[1] != 0.0f && src0[2] != 0.0f) {
            return;
        }

        // Loop inside the kernel of (2 x radius + 1)^2 size
        int dist = 2*radius*radius+1;
        int imin = std::max(i0 - radius, 0);
        int imax = std::min(i0 + radius, _width-1);
        int jmin = std::max(j0 - radius, 0);
        int jmax = std::min(j0 + radius, _height-1);
        for(int i=imin; i<=imax; ++i) {
            for(int j=jmin; j<=jmax; ++j) {

                float* src   = (*this)(i, j);
                int    ndist = pow(i-i0,2) + pow(j-j0,2);

                if(src[0] != 0.0f && src[1] != 0.0f  && src[2] != 0.0f && ndist < dist) {
                    dist = ndist;
                    std::memcpy(dest, src, _fpp*sizeof(float));
                }
            }
        }
    }


    /* Access the raw pointer
     */
    const float* ptr() const {
        return _pix;
    }

    /* Access width and height */
    int width() const { return _width; }
    int height() const { return _height; }

    /* Data */
    float* _pix;
    int _fpp, _width, _height;
};
}