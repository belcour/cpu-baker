// Ensure M_PI is defined
#define _USE_MATH_DEFINES

// Load TinyEXR and TinyOBJ
#define TINYOBJLOADER_IMPLEMENTATION
#include <tinyobj/tiny_obj_loader.h>

// cmdline includes
#include <cmdline/cmdline.h>

// STL includes
#include <string>
#include <iostream>

// Local includes
#include "image.h"
#include "raster.h"
#include "baker.h"

/* {TODO list}
 *   + Handle sideness due to blender exporting OBJ by default with 'forward' == '-x'.
 */

int main(int argc, char** argv) {

    /* Command line parsing */
    cmdline::parser args;
    args.add<std::string>("filename", 'f', "obj filename", true, "");
    args.add<std::string>("dither",   'd', "exr filename", false, "");
    args.add<std::string>("basename", 'b', "basename for output", false, "");
    args.add<int>("min-spp", 'm', "sample rate",      false, 0);
    args.add<int>("spp",     's', "sample rate",      false, 8);
    args.add<int>("res",     'r', "maps resolution",  false, 512);
    args.add<int>("passes",  'p', "number of passes", false, 10);
    args.add<bool>("stop",  'c', "use stopping criterion", false, false);
    args.parse_check(argc, argv);

    /* Load obj file */
    std::string inputfile = args.get<std::string>("filename");
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;

    std::string err;
    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, inputfile.c_str());

    if (!err.empty()) { // `err` may contain warning message.
        std::cerr << err << std::endl;
    }

    if (!ret) {
        EXIT_FAILURE;
    }

    /* Do nothing if no obj is present */
    if(shapes.size() == 0) {
        std::cerr << "No obj present in obj file" << std::endl;
        return EXIT_SUCCESS;
    }

    /* Obtain the output file resolution */
    int W = args.get<int>("res"), H = args.get<int>("res");
    baker::image img_n(W, H);
    baker::image img_p(W, H);
    baker::image img_ao(W, H);
    baker::image img_bn(W, H);

    /* Create the baking object */
    int spp     = args.get<int>("spp");
    int passes  = args.get<int>("passes");
    int min_spp = args.get<int>("min-spp");
    bool use_stopping_criterion = args.get<bool>("stop");
    baker::baker_config config;
    config._spp = spp;
    config._passes = passes;
    baker::baker_bentcone bbn(config, shapes, attrib);
    if(args.exist("dither")) {
        std::string dither_filename = args.get<std::string>("dither");
        bbn.load_dither(dither_filename);
    }

    /* Load the objects and fill the baker */
	unsigned int _nb_skipped_faces = 0;
	unsigned int _nb_total_faces = 0;

    /* Load the basename */
    std::string basename = "";
    if(args.exist("basename")) {
        basename = args.get<std::string>("basename");
    }

    // Loop over shapes
    for (size_t s = 0; s < shapes.size(); s++) {

        if(shapes[s].name.substr(0, 8) == "Untitled") {
            continue;
        }

        // Get the attributes
        std::string name = basename + shapes[s].name;

        // Reset buffers
        img_n.clear();
        img_p.clear();
        img_ao.clear();
        img_bn.clear();

        // Reset debug counters
        _nb_skipped_faces = 0;
		_nb_total_faces = shapes[s].mesh.num_face_vertices.size();

        /* Create the normal and position buffers */
        size_t index_offset = 0;
        for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
            int fv = shapes[s].mesh.num_face_vertices[f];

            // Raster the triangle in face
            const tinyobj::index_t& p1 = shapes[s].mesh.indices[index_offset + 0];
            const tinyobj::index_t& p2 = shapes[s].mesh.indices[index_offset + 1];
            const tinyobj::index_t& p3 = shapes[s].mesh.indices[index_offset + 2];
			if (p1.texcoord_index < 0 || p2.texcoord_index < 0 || p3.texcoord_index < 0) {
				++_nb_skipped_faces;
				continue;
			}

            baker::raster_triangle(p1, p2, p3, attrib, img_n, baker::raster_type::NORMAL);
            baker::raster_triangle(p1, p2, p3, attrib, img_p, baker::raster_type::POSITION);

            index_offset += fv;
        }
        std::cout << "[INFO] " << _nb_skipped_faces << " / " << _nb_total_faces << " faces skipped during raster." << std::endl;
        std::cout << "[INFO] Normal and Position maps computed." << std::endl;

        /* Bake the BentNormal and AmbiantOcclusion buffers */
        bbn.bake(img_p, img_n, &img_ao, &img_bn);
        std::cout << "[INFO] AO and BentNormals maps computed." << std::endl;

        /* Inflate some texels to avoid dark region in UV space du to interpolation */
        int radius = 5;
        img_p.inflate(radius);
        img_n.inflate(radius);
        img_ao.inflate(radius);
        img_bn.inflate(radius);

        img_n.save(name + std::string("_n.exr"), true);
        img_p.save(name + std::string("_p.exr"));
        img_ao.save(name + std::string("_ao.exr"));
        img_bn.save(name + std::string("_bn.exr"), true);

        std::cout << "[INFO] Inflated resulting maps" << std::endl;
    }

    return EXIT_SUCCESS;
}
