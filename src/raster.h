#pragma once

// TinyOBJ includes
#include <tinyobj/tiny_obj_loader.h>

// GLM includes
#include <glm/glm.hpp>

// Local includes
#include "image.h"

namespace baker {

enum raster_type {
    NORMAL,
    POSITION
};

void create_bbox(const tinyobj::index_t& p1,
                 const tinyobj::index_t& p2,
                 const tinyobj::index_t& p3,
                 const tinyobj::attrib_t& attrib,
                 baker::image& img,
                 int& min_i, int& max_i,
                 int& min_j, int& max_j) {

    // Init the bbox
    min_i = img.width();
    max_i = 0;
    min_j = img.height();
    max_j = 0;

    // Point p1
    const int p1i = img.width()  * attrib.texcoords[2*p1.texcoord_index+0];
    const int p1j = img.height() * attrib.texcoords[2*p1.texcoord_index+1];
    min_i = std::min<int>(min_i, p1i);
    max_i = std::max<int>(max_i, p1i);
    min_j = std::min<int>(min_j, p1j);
    max_j = std::max<int>(max_j, p1j);

    // Point p2
    const int p2i = img.width()  * attrib.texcoords[2*p2.texcoord_index+0];
    const int p2j = img.height() * attrib.texcoords[2*p2.texcoord_index+1];
    min_i = std::min<int>(min_i, p2i);
    max_i = std::max<int>(max_i, p2i);
    min_j = std::min<int>(min_j, p2j);
    max_j = std::max<int>(max_j, p2j);

    // Point p3
    const int p3i = img.width()  * attrib.texcoords[2*p3.texcoord_index+0];
    const int p3j = img.height() * attrib.texcoords[2*p3.texcoord_index+1];
    min_i = std::min<int>(min_i, p3i);
    max_i = std::max<int>(max_i, p3i);
    min_j = std::min<int>(min_j, p3j);
    max_j = std::max<int>(max_j, p3j);

    // Clamp the window to the img coordinates
    max_i = std::min<int>(max_i, img.width()-1);
    max_j = std::min<int>(max_j, img.height()-1);
}

void barycentric_coords(float  u,  float  v,
                        float  u1, float  v1,
                        float  u2, float  v2,
                        float  u3, float  v3, 
                        float* s,  float* t) {

    float detT = (v2-v3)*(u1-u3) + (u3-u2)*(v1-v3);
    (*s) = ((v2-v3)*(u-u3) + (u3-u2)*(v-v3)) / detT;
    (*t) = ((v3-v1)*(u-u3) + (u1-u3)*(v-v3)) / detT;
}

bool point_in_triangle(float u, float v,
                       const tinyobj::index_t& p1,
                       const tinyobj::index_t& p2,
                       const tinyobj::index_t& p3,
                       const tinyobj::attrib_t& attrib,
                       float* s_out = nullptr, float* t_out = nullptr) {

    // UV coordinates for p1, p2, and p3
    float u1 = attrib.texcoords[2*p1.texcoord_index+0];
    float v1 = attrib.texcoords[2*p1.texcoord_index+1];

    float u2 = attrib.texcoords[2*p2.texcoord_index+0];
    float v2 = attrib.texcoords[2*p2.texcoord_index+1];

    float u3 = attrib.texcoords[2*p3.texcoord_index+0];
    float v3 = attrib.texcoords[2*p3.texcoord_index+1];

    float s, t;
    barycentric_coords(u, v, u1, v1, u2, v2, u3, v3, &s, &t);

    if(s_out != nullptr) { (*s_out) = s; }
    if(t_out != nullptr) { (*t_out) = t; }

    return (s >= 0.f) && (t >= 0.f) && (s+t <= 1.f);
}

/* Rasterize a given triangle onto the image place
 */
void raster_triangle(const tinyobj::index_t& p1,
                     const tinyobj::index_t& p2,
                     const tinyobj::index_t& p3,
                     const tinyobj::attrib_t& attrib,
                     baker::image& img,
                     baker::raster_type type) {


    // Init the bbox corners and compute it using special function
    int min_i, max_i, min_j, max_j;
    create_bbox(p1, p2, p3, attrib, img, min_i, max_i, min_j, max_j);

    glm::vec3 v1, v2, v3;
    switch(type) {
        case  baker::raster_type::NORMAL:
            // Normal at vertex p1
            v1 = glm::vec3(attrib.normals[3*p1.normal_index+0],
                           attrib.normals[3*p1.normal_index+1],
                           attrib.normals[3*p1.normal_index+2]);

            // Normal at vertex p2
            v2 = glm::vec3(attrib.normals[3*p2.normal_index+0],
                           attrib.normals[3*p2.normal_index+1],
                           attrib.normals[3*p2.normal_index+2]);

            // Normal at vertex p3
            v3 = glm::vec3(attrib.normals[3*p3.normal_index+0],
                           attrib.normals[3*p3.normal_index+1],
                           attrib.normals[3*p3.normal_index+2]);
            break;
        case baker::raster_type::POSITION:
            // Normal at vertex p1
            v1 = glm::vec3(attrib.vertices[3*p1.vertex_index+0],
                           attrib.vertices[3*p1.vertex_index+1],
                           attrib.vertices[3*p1.vertex_index+2]);

            // Normal at vertex p2
            v2 = glm::vec3(attrib.vertices[3*p2.vertex_index+0],
                           attrib.vertices[3*p2.vertex_index+1],
                           attrib.vertices[3*p2.vertex_index+2]);

            // Normal at vertex p3
            v3 = glm::vec3(attrib.vertices[3*p3.vertex_index+0],
                           attrib.vertices[3*p3.vertex_index+1],
                           attrib.vertices[3*p3.vertex_index+2]);
            break;
    }

    // Loop over the bounding box and write the interpolated normal
    // {TODO: interpolate the normals}
	#pragma omp parallel for
    for(int j=min_j; j<=max_j; ++j) {
        for(int i=min_i; i<=max_i; ++i) {

            float u = (float)(i+0.5f) / (float)(img.width());
            float v = (float)(j+0.5f) / (float)(img.height());

            float s, t;
            if(point_in_triangle(u, v, p1, p2, p3, attrib, &s, &t)){
                glm::vec3 v = s*v1 + t*v2 + (1.f-s-t)*v3;
                if(type == baker::raster_type::NORMAL) {
                    v = glm::normalize(v);
                    img.writeNormal(i, j, v);
                } else {
                    img.writeVector(i, j, v);
                }
            }
        }
    }
}
}
